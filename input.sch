EESchema Schematic File Version 2
LIBS:Amplifier_Audio
LIBS:Amplifier_Operational
LIBS:Audio
LIBS:Device
LIBS:Diode
LIBS:Diode_Bridge
LIBS:Connector_Generic
LIBS:Connector_Generic_Shielded
LIBS:Connector_Specialized
LIBS:power
LIBS:Regulator_Linear
LIBS:Transistor_BJT
LIBS:Analog
LIBS:Relay
LIBS:Mechanical
LIBS:minima-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L NE5532 U301
U 1 1 5ABB3078
P 3600 2100
F 0 "U301" H 3600 2300 50  0000 L CNN
F 1 "NE5532" H 3600 1900 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 3600 2100 50  0001 C CNN
F 3 "" H 3600 2100 50  0001 C CNN
	1    3600 2100
	1    0    0    -1  
$EndComp
$Comp
L VSSA #PWR09
U 1 1 5ABB310B
P 3500 2500
F 0 "#PWR09" H 3500 2350 50  0001 C CNN
F 1 "VSSA" H 3500 2650 50  0000 C CNN
F 2 "" H 3500 2500 50  0001 C CNN
F 3 "" H 3500 2500 50  0001 C CNN
	1    3500 2500
	-1   0    0    1   
$EndComp
$Comp
L VDDA #PWR010
U 1 1 5ABB3133
P 3500 1700
F 0 "#PWR010" H 3500 1550 50  0001 C CNN
F 1 "VDDA" H 3500 1850 50  0000 C CNN
F 2 "" H 3500 1700 50  0001 C CNN
F 3 "" H 3500 1700 50  0001 C CNN
	1    3500 1700
	1    0    0    -1  
$EndComp
$Comp
L C C304
U 1 1 5ABB3781
P 2100 2550
F 0 "C304" H 2125 2650 50  0000 L CNN
F 1 "1n" H 2125 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 2138 2400 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 2100 2550 50  0001 C CNN
F 4 "100V" H 2150 2350 60  0000 L CNN "Voltage"
F 5 "NP0" H 2150 2250 60  0000 L CNN "Type"
F 6 "Farnell:1856276" H 2100 2550 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCMT21N102F101CT" H 2100 2550 60  0001 C CNN "Manufacturer"
	1    2100 2550
	1    0    0    -1  
$EndComp
$Comp
L C C303
U 1 1 5ABB3BB0
P 1600 2550
F 0 "C303" H 1625 2650 50  0000 L CNN
F 1 "220p" H 1625 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 1638 2400 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 1600 2550 50  0001 C CNN
F 4 "100V" H 1650 2350 60  0000 L CNN "Voltage"
F 5 "NP0" H 1650 2250 60  0000 L CNN "Type"
F 6 "Farnell:1856271" H 1600 2550 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCMT21N221F101CT" H 1600 2550 60  0001 C CNN "Manufacturer"
	1    1600 2550
	1    0    0    -1  
$EndComp
Text HLabel 1100 2000 0    60   Input ~ 0
inp
Text HLabel 7000 2300 2    60   Output ~ 0
outp
$Comp
L BAT54S D301
U 1 1 5ABB409D
P 1600 1200
F 0 "D301" H 1625 1050 50  0000 L CNN
F 1 "BAT54S" H 1350 1325 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 1675 1325 50  0001 L CNN
F 3 "" H 1480 1200 50  0001 C CNN
	1    1600 1200
	1    0    0    -1  
$EndComp
$Comp
L VDDA #PWR011
U 1 1 5ABB4219
P 2000 1200
F 0 "#PWR011" H 2000 1050 50  0001 C CNN
F 1 "VDDA" H 2000 1350 50  0000 C CNN
F 2 "" H 2000 1200 50  0001 C CNN
F 3 "" H 2000 1200 50  0001 C CNN
	1    2000 1200
	0    1    1    0   
$EndComp
$Comp
L VSSA #PWR012
U 1 1 5ABB42C6
P 1200 1200
F 0 "#PWR012" H 1200 1050 50  0001 C CNN
F 1 "VSSA" H 1200 1350 50  0000 C CNN
F 2 "" H 1200 1200 50  0001 C CNN
F 3 "" H 1200 1200 50  0001 C CNN
	1    1200 1200
	0    -1   -1   0   
$EndComp
Text HLabel 1100 3500 0    60   Input ~ 0
sgnd
Text HLabel 1100 5000 0    60   Input ~ 0
inn
$Comp
L NE5532 U301
U 2 1 5AC12F5C
P 3600 5100
F 0 "U301" H 3600 5300 50  0000 L CNN
F 1 "NE5532" H 3600 4900 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 3600 5100 50  0001 C CNN
F 3 "" H 3600 5100 50  0001 C CNN
	2    3600 5100
	1    0    0    -1  
$EndComp
$Comp
L VSSA #PWR013
U 1 1 5AC12F62
P 3500 5500
F 0 "#PWR013" H 3500 5350 50  0001 C CNN
F 1 "VSSA" H 3500 5650 50  0000 C CNN
F 2 "" H 3500 5500 50  0001 C CNN
F 3 "" H 3500 5500 50  0001 C CNN
	1    3500 5500
	-1   0    0    1   
$EndComp
$Comp
L VDDA #PWR014
U 1 1 5AC12F68
P 3500 4700
F 0 "#PWR014" H 3500 4550 50  0001 C CNN
F 1 "VDDA" H 3500 4850 50  0000 C CNN
F 2 "" H 3500 4700 50  0001 C CNN
F 3 "" H 3500 4700 50  0001 C CNN
	1    3500 4700
	1    0    0    -1  
$EndComp
$Comp
L BAT54S D302
U 1 1 5AC160DF
P 1600 5800
F 0 "D302" H 1625 5650 50  0000 L CNN
F 1 "BAT54S" H 1350 5925 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 1675 5925 50  0001 L CNN
F 3 "" H 1480 5800 50  0001 C CNN
	1    1600 5800
	1    0    0    1   
$EndComp
$Comp
L VDDA #PWR015
U 1 1 5AC160E5
P 2000 5800
F 0 "#PWR015" H 2000 5650 50  0001 C CNN
F 1 "VDDA" H 2000 5950 50  0000 C CNN
F 2 "" H 2000 5800 50  0001 C CNN
F 3 "" H 2000 5800 50  0001 C CNN
	1    2000 5800
	0    1    -1   0   
$EndComp
$Comp
L VSSA #PWR016
U 1 1 5AC160EB
P 1200 5800
F 0 "#PWR016" H 1200 5650 50  0001 C CNN
F 1 "VSSA" H 1200 5950 50  0000 C CNN
F 2 "" H 1200 5800 50  0001 C CNN
F 3 "" H 1200 5800 50  0001 C CNN
	1    1200 5800
	0    -1   1    0   
$EndComp
$Comp
L VDDA #PWR017
U 1 1 5AC1A043
P 8600 4900
F 0 "#PWR017" H 8600 4750 50  0001 C CNN
F 1 "VDDA" H 8600 5050 50  0000 C CNN
F 2 "" H 8600 4900 50  0001 C CNN
F 3 "" H 8600 4900 50  0001 C CNN
	1    8600 4900
	1    0    0    -1  
$EndComp
$Comp
L CP1 C310
U 1 1 5AC1A8B3
P 8600 5250
F 0 "C310" H 8625 5350 50  0000 L CNN
F 1 "22u" H 8625 5150 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 8600 5250 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2124421.pdf" H 8600 5250 50  0001 C CNN
F 4 "50V" H 8650 5050 60  0000 L CNN "Voltage"
F 5 "Farnell:2346268" H 8600 5250 60  0001 C CNN "Vendor"
F 6 "Rybicon:50YXJ22MTA5X11" H 8600 5250 60  0001 C CNN "Manufacturer"
	1    8600 5250
	1    0    0    -1  
$EndComp
$Comp
L VSSA #PWR018
U 1 1 5AC1BAB8
P 8600 6100
F 0 "#PWR018" H 8600 5950 50  0001 C CNN
F 1 "VSSA" H 8600 6250 50  0000 C CNN
F 2 "" H 8600 6100 50  0001 C CNN
F 3 "" H 8600 6100 50  0001 C CNN
	1    8600 6100
	-1   0    0    1   
$EndComp
$Comp
L GNDPWR #PWR019
U 1 1 5AC20ACD
P 8500 5500
F 0 "#PWR019" H 8500 5300 50  0001 C CNN
F 1 "GNDPWR" H 8500 5370 50  0000 C CNN
F 2 "" H 8500 5450 50  0001 C CNN
F 3 "" H 8500 5450 50  0001 C CNN
	1    8500 5500
	0    1    1    0   
$EndComp
$Comp
L C C308
U 1 1 5AC82F18
P 2100 4450
F 0 "C308" H 2125 4550 50  0000 L CNN
F 1 "1n" H 2125 4350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 2138 4300 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 2100 4450 50  0001 C CNN
F 4 "100V" H 2150 4250 60  0000 L CNN "Voltage"
F 5 "NP0" H 2150 4150 60  0000 L CNN "Type"
F 6 "Farnell:1856276" H 2100 4450 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCMT21N102F101CT" H 2100 4450 60  0001 C CNN "Manufacturer"
	1    2100 4450
	1    0    0    -1  
$EndComp
$Comp
L C C307
U 1 1 5AC88223
P 1600 4450
F 0 "C307" H 1625 4550 50  0000 L CNN
F 1 "220p" H 1625 4350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 1638 4300 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 1600 4450 50  0001 C CNN
F 4 "100V" H 1650 4250 60  0000 L CNN "Voltage"
F 5 "NP0" H 1650 4150 60  0000 L CNN "Type"
F 6 "Farnell:1856271" H 1600 4450 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCMT21N221F101CT" H 1600 4450 60  0001 C CNN "Manufacturer"
	1    1600 4450
	1    0    0    -1  
$EndComp
$Comp
L C C311
U 1 1 5AC8F1EF
P 8900 5250
F 0 "C311" H 8925 5350 50  0000 L CNN
F 1 "100n" H 8925 5150 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 8938 5100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2265830.pdf" H 8900 5250 50  0001 C CNN
F 4 "100V" H 8950 4950 60  0000 L CNN "Voltage"
F 5 "X7R" H 8950 5050 60  0000 L CNN "Type"
F 6 "Farnell:1856606" H 8900 5250 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCSH31B104K101CT" H 8900 5250 60  0001 C CNN "Manufacturer"
	1    8900 5250
	1    0    0    -1  
$EndComp
$Comp
L C C316
U 1 1 5AC8F906
P 8900 5750
F 0 "C316" H 8925 5850 50  0000 L CNN
F 1 "100n" H 8925 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 8938 5600 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2265830.pdf" H 8900 5750 50  0001 C CNN
F 4 "100V" H 8950 5450 60  0000 L CNN "Voltage"
F 5 "X7R" H 8950 5550 60  0000 L CNN "Type"
F 6 "Farnell:1856606" H 8900 5750 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCSH31B104K101CT" H 8900 5750 60  0001 C CNN "Manufacturer"
	1    8900 5750
	1    0    0    -1  
$EndComp
$Comp
L C C318
U 1 1 5AC8FA0C
P 9500 5750
F 0 "C318" H 9525 5850 50  0000 L CNN
F 1 "100n" H 9525 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 9538 5600 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2265830.pdf" H 9500 5750 50  0001 C CNN
F 4 "100V" H 9550 5450 60  0000 L CNN "Voltage"
F 5 "X7R" H 9550 5550 60  0000 L CNN "Type"
F 6 "Farnell:1856606" H 9500 5750 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCSH31B104K101CT" H 9500 5750 60  0001 C CNN "Manufacturer"
	1    9500 5750
	1    0    0    -1  
$EndComp
$Comp
L C C313
U 1 1 5AC90018
P 9500 5250
F 0 "C313" H 9525 5350 50  0000 L CNN
F 1 "100n" H 9525 5150 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 9538 5100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2265830.pdf" H 9500 5250 50  0001 C CNN
F 4 "100V" H 9550 4950 60  0000 L CNN "Voltage"
F 5 "X7R" H 9550 5050 60  0000 L CNN "Type"
F 6 "Farnell:1856606" H 9500 5250 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCSH31B104K101CT" H 9500 5250 60  0001 C CNN "Manufacturer"
	1    9500 5250
	1    0    0    -1  
$EndComp
$Comp
L VSSA #PWR020
U 1 1 5AC07AF9
P 5700 2700
F 0 "#PWR020" H 5700 2550 50  0001 C CNN
F 1 "VSSA" H 5700 2850 50  0000 C CNN
F 2 "" H 5700 2700 50  0001 C CNN
F 3 "" H 5700 2700 50  0001 C CNN
	1    5700 2700
	-1   0    0    1   
$EndComp
$Comp
L VDDA #PWR021
U 1 1 5AC07AC7
P 5700 1900
F 0 "#PWR021" H 5700 1750 50  0001 C CNN
F 1 "VDDA" H 5700 2050 50  0000 C CNN
F 2 "" H 5700 1900 50  0001 C CNN
F 3 "" H 5700 1900 50  0001 C CNN
	1    5700 1900
	1    0    0    -1  
$EndComp
$Comp
L NE5532 U302
U 1 1 5AC079D1
P 5800 2300
F 0 "U302" H 5800 2500 50  0000 L CNN
F 1 "NE5532" H 5800 2100 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 5800 2300 50  0001 C CNN
F 3 "" H 5800 2300 50  0001 C CNN
	1    5800 2300
	1    0    0    -1  
$EndComp
$Comp
L CP1 C315
U 1 1 5AD5AF84
P 8600 5750
F 0 "C315" H 8625 5850 50  0000 L CNN
F 1 "22u" H 8625 5650 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 8600 5750 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2124421.pdf" H 8600 5750 50  0001 C CNN
F 4 "50V" H 8650 5550 60  0000 L CNN "Voltage"
F 5 "Farnell:2346268" H 8600 5750 60  0001 C CNN "Vendor"
F 6 "Rybicon:50YXJ22MTA5X11" H 8600 5750 60  0001 C CNN "Manufacturer"
	1    8600 5750
	1    0    0    -1  
$EndComp
$Comp
L CP1 C317
U 1 1 5AD5B046
P 9200 5750
F 0 "C317" H 9225 5850 50  0000 L CNN
F 1 "22u" H 9225 5650 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 9200 5750 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2124421.pdf" H 9200 5750 50  0001 C CNN
F 4 "50V" H 9250 5550 60  0000 L CNN "Voltage"
F 5 "Farnell:2346268" H 9200 5750 60  0001 C CNN "Vendor"
F 6 "Rybicon:50YXJ22MTA5X11" H 9200 5750 60  0001 C CNN "Manufacturer"
	1    9200 5750
	1    0    0    -1  
$EndComp
$Comp
L CP1 C312
U 1 1 5AD5B10B
P 9200 5250
F 0 "C312" H 9225 5350 50  0000 L CNN
F 1 "22u" H 9225 5150 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 9200 5250 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2124421.pdf" H 9200 5250 50  0001 C CNN
F 4 "50V" H 9250 5050 60  0000 L CNN "Voltage"
F 5 "Farnell:2346268" H 9200 5250 60  0001 C CNN "Vendor"
F 6 "Rybicon:50YXJ22MTA5X11" H 9200 5250 60  0001 C CNN "Manufacturer"
	1    9200 5250
	1    0    0    -1  
$EndComp
$Comp
L CP1 C309
U 1 1 5AD5B6D6
P 2650 5000
F 0 "C309" H 2675 5100 50  0000 L CNN
F 1 "22u" H 2675 4900 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2650 5000 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2124421.pdf" H 2650 5000 50  0001 C CNN
F 4 "50V" H 2700 4800 60  0000 L CNN "Voltage"
F 5 "Farnell:2346268" H 2650 5000 60  0001 C CNN "Vendor"
F 6 "Rybicon:50YXJ22MTA5X11" H 2650 5000 60  0001 C CNN "Manufacturer"
	1    2650 5000
	0    1    1    0   
$EndComp
$Comp
L CP1 C302
U 1 1 5AD5B7AE
P 2650 2000
F 0 "C302" H 2675 2100 50  0000 L CNN
F 1 "22u" H 2675 1900 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 2650 2000 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2124421.pdf" H 2650 2000 50  0001 C CNN
F 4 "50V" H 2700 1800 60  0000 L CNN "Voltage"
F 5 "Farnell:2346268" H 2650 2000 60  0001 C CNN "Vendor"
F 6 "Rybicon:50YXJ22MTA5X11" H 2650 2000 60  0001 C CNN "Manufacturer"
	1    2650 2000
	0    1    1    0   
$EndComp
$Comp
L R R302
U 1 1 5AD6B4EC
P 1350 2000
F 0 "R302" V 1430 2000 50  0000 C CNN
F 1 "100" V 1350 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 1280 2000 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 1350 2000 50  0001 C CNN
F 4 "Farnell:1717738" V 1350 2000 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB101V" V 1350 2000 60  0001 C CNN "Manufacturer"
F 6 "MF" V 1600 2000 60  0000 C CNN "Type"
F 7 "1/4" V 1500 2000 60  0000 C CNN "Power"
	1    1350 2000
	0    1    1    0   
$EndComp
$Comp
L R R303
U 1 1 5AD70334
P 1850 2000
F 0 "R303" V 1930 2000 50  0000 C CNN
F 1 "100" V 1850 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 1780 2000 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 1850 2000 50  0001 C CNN
F 4 "Farnell:1717738" V 1850 2000 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB101V" V 1850 2000 60  0001 C CNN "Manufacturer"
F 6 "MF" V 2100 2000 60  0000 C CNN "Type"
F 7 "1/4" V 2000 2000 60  0000 C CNN "Power"
	1    1850 2000
	0    1    1    0   
$EndComp
$Comp
L R R315
U 1 1 5AD70400
P 1350 5000
F 0 "R315" V 1430 5000 50  0000 C CNN
F 1 "100" V 1350 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 1280 5000 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 1350 5000 50  0001 C CNN
F 4 "Farnell:1717738" V 1350 5000 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB101V" V 1350 5000 60  0001 C CNN "Manufacturer"
F 6 "MF" V 1600 5000 60  0000 C CNN "Type"
F 7 "1/4" V 1500 5000 60  0000 C CNN "Power"
	1    1350 5000
	0    1    1    0   
$EndComp
$Comp
L R R316
U 1 1 5AD704D9
P 1850 5000
F 0 "R316" V 1930 5000 50  0000 C CNN
F 1 "100" V 1850 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 1780 5000 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 1850 5000 50  0001 C CNN
F 4 "Farnell:1717738" V 1850 5000 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB101V" V 1850 5000 60  0001 C CNN "Manufacturer"
F 6 "MF" V 2100 5000 60  0000 C CNN "Type"
F 7 "1/4" V 2000 5000 60  0000 C CNN "Power"
	1    1850 5000
	0    1    1    0   
$EndComp
$Comp
L R R308
U 1 1 5AD70DED
P 2900 2550
F 0 "R308" V 2980 2550 50  0000 C CNN
F 1 "11k" V 2900 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 2830 2550 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 2900 2550 50  0001 C CNN
F 4 "Farnell:2484802" V 2900 2550 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB113V" V 2900 2550 60  0001 C CNN "Manufacturer"
F 6 "MF" V 3150 2550 60  0000 C CNN "Type"
F 7 "1/4" V 3050 2550 60  0000 C CNN "Power"
	1    2900 2550
	1    0    0    -1  
$EndComp
$Comp
L R R307
U 1 1 5AD713B4
P 2400 2550
F 0 "R307" V 2480 2550 50  0000 C CNN
F 1 "100k" V 2400 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 2330 2550 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 2400 2550 50  0001 C CNN
F 4 "Farnell:1717758" V 2400 2550 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB104V" V 2400 2550 60  0001 C CNN "Manufacturer"
F 6 "MF" V 2650 2550 60  0000 C CNN "Type"
F 7 "1/4" V 2550 2550 60  0000 C CNN "Power"
	1    2400 2550
	1    0    0    -1  
$EndComp
$Comp
L R R311
U 1 1 5AD71586
P 2400 4450
F 0 "R311" V 2480 4450 50  0000 C CNN
F 1 "100k" V 2400 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 2330 4450 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 2400 4450 50  0001 C CNN
F 4 "Farnell:1717758" V 2400 4450 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB104V" V 2400 4450 60  0001 C CNN "Manufacturer"
F 6 "MF" V 2650 4450 60  0000 C CNN "Type"
F 7 "1/4" V 2550 4450 60  0000 C CNN "Power"
	1    2400 4450
	1    0    0    -1  
$EndComp
$Comp
L R R312
U 1 1 5AD7166D
P 2900 4450
F 0 "R312" V 2980 4450 50  0000 C CNN
F 1 "11k" V 2900 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 2830 4450 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 2900 4450 50  0001 C CNN
F 4 "Farnell:2484802" V 2900 4450 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB113V" V 2900 4450 60  0001 C CNN "Manufacturer"
F 6 "MF" V 3150 4450 60  0000 C CNN "Type"
F 7 "1/4" V 3050 4450 60  0000 C CNN "Power"
	1    2900 4450
	1    0    0    -1  
$EndComp
$Comp
L R R305
U 1 1 5AD71CD8
P 6450 2300
F 0 "R305" V 6530 2300 50  0000 C CNN
F 1 "49.9" V 6450 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 6380 2300 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 6450 2300 50  0001 C CNN
F 4 "Farnell:2095158" V 6450 2300 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB49R9V" V 6450 2300 60  0001 C CNN "Manufacturer"
F 6 "MF" V 6700 2300 60  0000 C CNN "Type"
F 7 "1/4" V 6600 2300 60  0000 C CNN "Power"
	1    6450 2300
	0    1    1    0   
$EndComp
$Comp
L C C319
U 1 1 5ADDA1C4
P 10400 5750
F 0 "C319" H 10425 5850 50  0000 L CNN
F 1 "100n" H 10425 5650 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 10438 5600 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2265830.pdf" H 10400 5750 50  0001 C CNN
F 4 "100V" H 10450 5450 60  0000 L CNN "Voltage"
F 5 "X7R" H 10450 5550 60  0000 L CNN "Type"
F 6 "Farnell:1856606" H 10400 5750 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCSH31B104K101CT" H 10400 5750 60  0001 C CNN "Manufacturer"
	1    10400 5750
	1    0    0    -1  
$EndComp
$Comp
L C C314
U 1 1 5ADDA1CE
P 10400 5250
F 0 "C314" H 10425 5350 50  0000 L CNN
F 1 "100n" H 10425 5150 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.24x1.80mm_HandSolder" H 10438 5100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2265830.pdf" H 10400 5250 50  0001 C CNN
F 4 "100V" H 10450 4950 60  0000 L CNN "Voltage"
F 5 "X7R" H 10450 5050 60  0000 L CNN "Type"
F 6 "Farnell:1856606" H 10400 5250 60  0001 C CNN "Vendor"
F 7 "Multicomp:MCSH31B104K101CT" H 10400 5250 60  0001 C CNN "Manufacturer"
	1    10400 5250
	1    0    0    -1  
$EndComp
$Comp
L GNDPWR #PWR022
U 1 1 5ADDA21C
P 10300 5500
F 0 "#PWR022" H 10300 5300 50  0001 C CNN
F 1 "GNDPWR" H 10300 5370 50  0000 C CNN
F 2 "" H 10300 5450 50  0001 C CNN
F 3 "" H 10300 5450 50  0001 C CNN
	1    10300 5500
	0    1    1    0   
$EndComp
$Comp
L VDDA #PWR023
U 1 1 5ADDA28D
P 10400 4900
F 0 "#PWR023" H 10400 4750 50  0001 C CNN
F 1 "VDDA" H 10400 5050 50  0000 C CNN
F 2 "" H 10400 4900 50  0001 C CNN
F 3 "" H 10400 4900 50  0001 C CNN
	1    10400 4900
	1    0    0    -1  
$EndComp
$Comp
L VSSA #PWR024
U 1 1 5ADDA2FE
P 10400 6100
F 0 "#PWR024" H 10400 5950 50  0001 C CNN
F 1 "VSSA" H 10400 6250 50  0000 C CNN
F 2 "" H 10400 6100 50  0001 C CNN
F 3 "" H 10400 6100 50  0001 C CNN
	1    10400 6100
	-1   0    0    1   
$EndComp
$Comp
L R R304
U 1 1 5ADDB186
P 5050 2100
F 0 "R304" V 5130 2100 50  0000 C CNN
F 1 "2k" V 5050 2100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 4980 2100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5050 2100 50  0001 C CNN
F 4 "Farnell:2484813" V 5050 2100 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5050 2100 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5300 2100 60  0000 C CNN "Type"
F 7 "1/4" V 5200 2100 60  0000 C CNN "Power"
	1    5050 2100
	0    1    1    0   
$EndComp
$Comp
L R R306
U 1 1 5ADDBCB3
P 5050 2500
F 0 "R306" V 5130 2500 50  0000 C CNN
F 1 "2k" V 5050 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 4980 2500 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5050 2500 50  0001 C CNN
F 4 "Farnell:2484813" V 5050 2500 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5050 2500 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5300 2500 60  0000 C CNN "Type"
F 7 "1/4" V 5200 2500 60  0000 C CNN "Power"
	1    5050 2500
	0    1    1    0   
$EndComp
$Comp
L R R309
U 1 1 5ADDBD5B
P 5550 3100
F 0 "R309" V 5630 3100 50  0000 C CNN
F 1 "2k" V 5550 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 5480 3100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5550 3100 50  0001 C CNN
F 4 "Farnell:2484813" V 5550 3100 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5550 3100 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5800 3100 60  0000 C CNN "Type"
F 7 "1/4" V 5700 3100 60  0000 C CNN "Power"
	1    5550 3100
	0    1    1    0   
$EndComp
$Comp
L R R301
U 1 1 5ADDBE12
P 5550 1500
F 0 "R301" V 5630 1500 50  0000 C CNN
F 1 "2k" V 5550 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 5480 1500 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5550 1500 50  0001 C CNN
F 4 "Farnell:2484813" V 5550 1500 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5550 1500 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5800 1500 60  0000 C CNN "Type"
F 7 "1/4" V 5700 1500 60  0000 C CNN "Power"
	1    5550 1500
	0    1    1    0   
$EndComp
Text HLabel 6200 1500 2    60   Input ~ 0
sgnd
$Comp
L C C301
U 1 1 5ADDC286
P 5550 1200
F 0 "C301" H 5575 1300 50  0000 L CNN
F 1 "22p" H 5575 1100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 5588 1050 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 5550 1200 50  0001 C CNN
F 4 "100V" H 5600 1000 60  0000 L CNN "Voltage"
F 5 "NP0" H 5600 900 60  0000 L CNN "Type"
F 6 "Farnell:317500" H 5550 1200 60  0001 C CNN "Vendor"
F 7 "AVX:08051A220JAT2A" H 5550 1200 60  0001 C CNN "Manufacturer"
	1    5550 1200
	0    -1   -1   0   
$EndComp
Text HLabel 6200 1200 2    60   Input ~ 0
sgnd
$Comp
L C C305
U 1 1 5ADDC6F5
P 5550 3400
F 0 "C305" H 5575 3500 50  0000 L CNN
F 1 "22p" H 5575 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 5588 3250 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 5550 3400 50  0001 C CNN
F 4 "100V" H 5600 3200 60  0000 L CNN "Voltage"
F 5 "NP0" H 5600 3100 60  0000 L CNN "Type"
F 6 "Farnell:317500" H 5550 3400 60  0001 C CNN "Vendor"
F 7 "AVX:08051A220JAT2A" H 5550 3400 60  0001 C CNN "Manufacturer"
	1    5550 3400
	0    -1   -1   0   
$EndComp
$Comp
L VSSA #PWR025
U 1 1 5ADDD972
P 5700 5300
F 0 "#PWR025" H 5700 5150 50  0001 C CNN
F 1 "VSSA" H 5700 5450 50  0000 C CNN
F 2 "" H 5700 5300 50  0001 C CNN
F 3 "" H 5700 5300 50  0001 C CNN
	1    5700 5300
	-1   0    0    1   
$EndComp
$Comp
L VDDA #PWR026
U 1 1 5ADDD978
P 5700 4500
F 0 "#PWR026" H 5700 4350 50  0001 C CNN
F 1 "VDDA" H 5700 4650 50  0000 C CNN
F 2 "" H 5700 4500 50  0001 C CNN
F 3 "" H 5700 4500 50  0001 C CNN
	1    5700 4500
	1    0    0    -1  
$EndComp
$Comp
L NE5532 U302
U 2 1 5ADDD97E
P 5800 4900
F 0 "U302" H 5800 5100 50  0000 L CNN
F 1 "NE5532" H 5800 4700 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 5800 4900 50  0001 C CNN
F 3 "" H 5800 4900 50  0001 C CNN
	2    5800 4900
	1    0    0    -1  
$EndComp
$Comp
L R R314
U 1 1 5ADDD988
P 6450 4900
F 0 "R314" V 6530 4900 50  0000 C CNN
F 1 "49.9" V 6450 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 6380 4900 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 6450 4900 50  0001 C CNN
F 4 "Farnell:2095158" V 6450 4900 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB49R9V" V 6450 4900 60  0001 C CNN "Manufacturer"
F 6 "MF" V 6700 4900 60  0000 C CNN "Type"
F 7 "1/4" V 6600 4900 60  0000 C CNN "Power"
	1    6450 4900
	0    1    1    0   
$EndComp
$Comp
L R R313
U 1 1 5ADDD992
P 5050 4700
F 0 "R313" V 5130 4700 50  0000 C CNN
F 1 "2k" V 5050 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 4980 4700 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5050 4700 50  0001 C CNN
F 4 "Farnell:2484813" V 5050 4700 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5050 4700 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5300 4700 60  0000 C CNN "Type"
F 7 "1/4" V 5200 4700 60  0000 C CNN "Power"
	1    5050 4700
	0    1    1    0   
$EndComp
$Comp
L R R317
U 1 1 5ADDD99C
P 5050 5100
F 0 "R317" V 5130 5100 50  0000 C CNN
F 1 "2k" V 5050 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 4980 5100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5050 5100 50  0001 C CNN
F 4 "Farnell:2484813" V 5050 5100 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5050 5100 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5300 5100 60  0000 C CNN "Type"
F 7 "1/4" V 5200 5100 60  0000 C CNN "Power"
	1    5050 5100
	0    1    1    0   
$EndComp
$Comp
L R R318
U 1 1 5ADDD9A6
P 5550 5700
F 0 "R318" V 5630 5700 50  0000 C CNN
F 1 "2k" V 5550 5700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 5480 5700 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5550 5700 50  0001 C CNN
F 4 "Farnell:2484813" V 5550 5700 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5550 5700 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5800 5700 60  0000 C CNN "Type"
F 7 "1/4" V 5700 5700 60  0000 C CNN "Power"
	1    5550 5700
	0    1    1    0   
$EndComp
$Comp
L R R310
U 1 1 5ADDD9B0
P 5550 4100
F 0 "R310" V 5630 4100 50  0000 C CNN
F 1 "2k" V 5550 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 5480 4100 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 5550 4100 50  0001 C CNN
F 4 "Farnell:2484813" V 5550 4100 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8ARB202V" V 5550 4100 60  0001 C CNN "Manufacturer"
F 6 "MF" V 5800 4100 60  0000 C CNN "Type"
F 7 "1/4" V 5700 4100 60  0000 C CNN "Power"
	1    5550 4100
	0    1    1    0   
$EndComp
Text HLabel 6200 4100 2    60   Input ~ 0
sgnd
$Comp
L C C306
U 1 1 5ADDD9CA
P 5550 3800
F 0 "C306" H 5575 3900 50  0000 L CNN
F 1 "22p" H 5575 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 5588 3650 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 5550 3800 50  0001 C CNN
F 4 "100V" H 5600 3600 60  0000 L CNN "Voltage"
F 5 "NP0" H 5600 3500 60  0000 L CNN "Type"
F 6 "Farnell:317500" H 5550 3800 60  0001 C CNN "Vendor"
F 7 "AVX:08051A220JAT2A" H 5550 3800 60  0001 C CNN "Manufacturer"
	1    5550 3800
	0    -1   -1   0   
$EndComp
Text HLabel 6200 3800 2    60   Input ~ 0
sgnd
$Comp
L C C320
U 1 1 5ADDD9D9
P 5550 6000
F 0 "C320" H 5575 6100 50  0000 L CNN
F 1 "22p" H 5575 5900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.50mm_HandSolder" H 5588 5850 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/1825496.pdf" H 5550 6000 50  0001 C CNN
F 4 "100V" H 5600 5800 60  0000 L CNN "Voltage"
F 5 "NP0" H 5600 5700 60  0000 L CNN "Type"
F 6 "Farnell:317500" H 5550 6000 60  0001 C CNN "Vendor"
F 7 "AVX:08051A220JAT2A" H 5550 6000 60  0001 C CNN "Manufacturer"
	1    5550 6000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3500 2500 3500 2400
Wire Wire Line
	3500 1800 3500 1700
Wire Wire Line
	3300 2200 3200 2200
Wire Wire Line
	3200 2200 3200 2800
Wire Wire Line
	2800 2000 3300 2000
Wire Wire Line
	2900 3200 2900 2700
Wire Wire Line
	2000 2000 2500 2000
Connection ~ 2900 2000
Wire Wire Line
	2100 3200 2100 2700
Connection ~ 2400 2000
Wire Wire Line
	1500 2000 1700 2000
Wire Wire Line
	1600 1400 1600 2400
Connection ~ 1600 2000
Connection ~ 2100 2000
Wire Wire Line
	1100 2000 1200 2000
Wire Wire Line
	1200 1200 1300 1200
Wire Wire Line
	1900 1200 2000 1200
Connection ~ 1600 3500
Connection ~ 1600 3200
Wire Wire Line
	2400 3200 2900 3200
Wire Wire Line
	2100 3200 1600 3200
Connection ~ 2400 3500
Connection ~ 2400 3200
Wire Wire Line
	1600 2700 1600 4300
Wire Wire Line
	3200 2800 4000 2800
Wire Wire Line
	3900 2100 4900 2100
Wire Wire Line
	4000 2800 4000 2100
Connection ~ 4000 2100
Wire Wire Line
	3500 5500 3500 5400
Wire Wire Line
	3500 4800 3500 4700
Wire Wire Line
	3300 5200 3200 5200
Wire Wire Line
	3200 5200 3200 5800
Wire Wire Line
	3200 5800 4000 5800
Wire Wire Line
	4000 5800 4000 5100
Wire Wire Line
	3900 5100 4900 5100
Connection ~ 4000 5100
Wire Wire Line
	2400 2700 2400 4300
Wire Wire Line
	2900 4300 2900 3800
Wire Wire Line
	2900 3800 2400 3800
Connection ~ 2400 3800
Wire Wire Line
	2100 4300 2100 3800
Wire Wire Line
	2100 3800 1600 3800
Connection ~ 1600 3800
Wire Wire Line
	2800 5000 3300 5000
Connection ~ 2900 5000
Wire Wire Line
	2000 5000 2500 5000
Connection ~ 2400 5000
Wire Wire Line
	2100 4600 2100 5000
Connection ~ 2100 5000
Wire Wire Line
	1500 5000 1700 5000
Wire Wire Line
	1100 5000 1200 5000
Wire Wire Line
	1600 4600 1600 5600
Connection ~ 1600 5000
Wire Wire Line
	1200 5800 1300 5800
Wire Wire Line
	1900 5800 2000 5800
Wire Wire Line
	8600 5900 8600 6100
Wire Wire Line
	8900 6000 8900 5900
Connection ~ 8600 6000
Wire Wire Line
	9200 6000 9200 5900
Connection ~ 8900 6000
Wire Wire Line
	9500 6000 9500 5900
Connection ~ 9200 6000
Wire Wire Line
	8600 5400 8600 5600
Wire Wire Line
	9500 5400 9500 5600
Wire Wire Line
	9200 5400 9200 5600
Wire Wire Line
	8900 5400 8900 5600
Connection ~ 8600 5500
Wire Wire Line
	8600 4900 8600 5100
Connection ~ 8600 5000
Connection ~ 9500 5500
Connection ~ 9200 5500
Connection ~ 8900 5500
Wire Wire Line
	8900 5000 8900 5100
Connection ~ 8900 5000
Wire Wire Line
	9200 5000 9200 5100
Connection ~ 9200 5000
Wire Wire Line
	9500 5000 9500 5100
Wire Wire Line
	2100 2400 2100 2000
Wire Wire Line
	5700 2000 5700 1900
Wire Wire Line
	5700 2700 5700 2600
Wire Wire Line
	5300 2200 5500 2200
Wire Wire Line
	6100 2300 6300 2300
Wire Wire Line
	2900 2400 2900 2000
Wire Wire Line
	2400 2400 2400 2000
Wire Wire Line
	2400 4600 2400 5000
Wire Wire Line
	2900 4600 2900 5000
Wire Wire Line
	8600 5000 9500 5000
Wire Wire Line
	8600 6000 9500 6000
Wire Wire Line
	1100 3500 2400 3500
Wire Wire Line
	8500 5500 9500 5500
Wire Wire Line
	10400 5100 10400 4900
Wire Wire Line
	10400 5400 10400 5600
Wire Wire Line
	10300 5500 10400 5500
Connection ~ 10400 5500
Wire Wire Line
	10400 6100 10400 5900
Wire Wire Line
	5300 1200 5300 2200
Wire Wire Line
	5300 2100 5200 2100
Wire Wire Line
	5200 2500 5300 2500
Wire Wire Line
	5300 2400 5300 3400
Wire Wire Line
	5300 2400 5500 2400
Wire Wire Line
	5300 1500 5400 1500
Connection ~ 5300 2100
Wire Wire Line
	5300 3100 5400 3100
Connection ~ 5300 2500
Wire Wire Line
	6200 3100 5700 3100
Connection ~ 6200 2300
Wire Wire Line
	5300 1200 5400 1200
Connection ~ 5300 1500
Wire Wire Line
	5700 1200 6200 1200
Wire Wire Line
	5700 1500 6200 1500
Wire Wire Line
	5300 3400 5400 3400
Connection ~ 5300 3100
Wire Wire Line
	6200 3400 5700 3400
Connection ~ 6200 3100
Wire Wire Line
	6200 2300 6200 3400
Wire Wire Line
	5700 4600 5700 4500
Wire Wire Line
	5700 5300 5700 5200
Wire Wire Line
	5300 4800 5500 4800
Wire Wire Line
	6100 4900 6300 4900
Wire Wire Line
	5300 3800 5300 4800
Wire Wire Line
	5300 4700 5200 4700
Wire Wire Line
	5200 5100 5300 5100
Wire Wire Line
	5300 5000 5300 6000
Wire Wire Line
	5300 5000 5500 5000
Wire Wire Line
	5300 4100 5400 4100
Connection ~ 5300 4700
Wire Wire Line
	5300 5700 5400 5700
Connection ~ 5300 5100
Wire Wire Line
	6200 5700 5700 5700
Connection ~ 6200 4900
Wire Wire Line
	5300 3800 5400 3800
Connection ~ 5300 4100
Wire Wire Line
	5700 3800 6200 3800
Wire Wire Line
	5700 4100 6200 4100
Wire Wire Line
	5300 6000 5400 6000
Connection ~ 5300 5700
Wire Wire Line
	6200 6000 5700 6000
Connection ~ 6200 5700
Wire Wire Line
	6200 4900 6200 6000
Wire Wire Line
	4900 4700 4600 4700
Wire Wire Line
	4600 4700 4600 2100
Connection ~ 4600 2100
Wire Wire Line
	4300 5100 4300 2500
Wire Wire Line
	4300 2500 4900 2500
Connection ~ 4300 5100
Wire Wire Line
	6600 4900 6700 4900
Wire Wire Line
	6700 4900 6700 2300
Wire Wire Line
	6600 2300 7000 2300
Connection ~ 6700 2300
Text HLabel 7300 6300 2    60   Output ~ 0
outn
Text HLabel 6550 6300 0    60   Input ~ 0
sgnd
$Comp
L R R320
U 1 1 5AE7C4EE
P 6950 6300
F 0 "R320" V 7030 6300 50  0000 C CNN
F 1 "49.9" V 6950 6300 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 6880 6300 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 6950 6300 50  0001 C CNN
F 4 "Farnell:2095158" V 6950 6300 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB49R9V" V 6950 6300 60  0001 C CNN "Manufacturer"
F 6 "MF" V 7200 6300 60  0000 C CNN "Type"
F 7 "1/4" V 7100 6300 60  0000 C CNN "Power"
	1    6950 6300
	0    1    1    0   
$EndComp
$Comp
L R R319
U 1 1 5AE7C6C1
P 6950 5700
F 0 "R319" V 7030 5700 50  0000 C CNN
F 1 "49.9" V 6950 5700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.24x1.80mm_HandSolder" V 6880 5700 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2052875.pdf" H 6950 5700 50  0001 C CNN
F 4 "Farnell:2095158" V 6950 5700 60  0001 C CNN "Vendor"
F 5 "PANASONIC:ERA8AEB49R9V" V 6950 5700 60  0001 C CNN "Manufacturer"
F 6 "MF" V 7200 5700 60  0000 C CNN "Type"
F 7 "1/4" V 7100 5700 60  0000 C CNN "Power"
	1    6950 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	7100 6300 7300 6300
Wire Wire Line
	7100 5700 7200 5700
Wire Wire Line
	7200 5700 7200 6300
Connection ~ 7200 6300
Wire Wire Line
	6550 6300 6800 6300
Wire Wire Line
	6800 5700 6700 5700
Wire Wire Line
	6700 5700 6700 6300
Connection ~ 6700 6300
$EndSCHEMATC
